$(function() {

	var chosenQuizName = '';
	var generated_Quiz = [];		
	var answerPool = [];
    var selectedAnswers = [];
    var questionIndex = 0;
    var correctAnswerCount = 0;
    var pointsCollected = 0;
    var maxPoints = 0;
    var isSaved = 0;
    var storage = localStorage;

    var selectQuizElem = $('#select_quiz');
    var startQuizA = $('#quiz_a');
    var startQuizB = $('#quiz_b');
    var header = $('h3');    
    var answersElem = $('#answers');
    var answerElem = $();
    var previousButton = $('#previous'); previousButton.hide();
    var nextButton = $('#next'); nextButton.hide(); 
    var showResultsButton = $('#show_results'); showResultsButton.hide();
    var resultTable = $('#result_table'); resultTable.hide();   
    var startOverButton = $('#start_over'); startOverButton.hide();
    var saveResultsButton = $('#save_results'); saveResultsButton.hide();
    var submitResultsElem = $('#submit_results'); submitResultsElem.hide();
    var showSavedResultsButton = $('#saved_results'); showSavedResultsButton.hide();


    //INTRODUCTION

    // The questions are changed by changing global variable questionIndex;

    // Depending on what the user has done, there are corresponding quiz states,
    // which are saved to localstorage.

    // User input is saved in localstorage and together with quizStage state is retrieved
    // on script load, user input and quizStage is then used to recreate the enviroment
    // that was before the user left/refreshed the page.


    // #########

    // Checks if storage exists.

    // If storage exists it collects the storage data, activates the button handlers
    // and initiates code accordingly depending on where user was before the pages 
    //closed/refreshed.

    // If storage doesn't exist it will simply initiate click hanlders.

    function checkSession () {   	    	
    	if (storage.getItem('generated_Quiz') == null) { 
    		startButtonHandlers();
	    } else {	    	
	    	generated_Quiz = JSON.parse(storage.getItem('generated_Quiz'));
    		chosenQuizName = storage.getItem('chosenQuizName');
	    	randomQuestionOrder = JSON.parse(storage.getItem('randomQuestionOrder'));
	    	selectedAnswers = JSON.parse(storage.getItem('selectedAnswers')) || []; 
	    	questionIndex = JSON.parse(storage.getItem('questionIndex'));
	    	correctAnswerCount = storage.getItem('correctAnswerCount');
	    	pointsCollected = storage.getItem('pointsCollected');
	    	maxPoints = storage.getItem('maxPoints');
	    	isSaved = storage.getItem('isSaved');

	    	startButtonHandlers();

	    	switch(storage.getItem('quizStage')) {
	    		case 'inProgress':
	    			createQuestion(); 			
	    			break;
	    		case 'generateResults':
	    			generateResults();
	    			break;
	    		case 'saveResults':
	    			saveResults();
	    			break;
	    		case 'showSavedResults':
	    			showSavedResults();
	    			break;
	    	} 	
	    }
    }

    // Out of the given quiz array pool of answers out of correct answers of each
    // question is created which is then used as a base for creating wrong
    // answers in the quiz.

   	// Generates random question/answer sequence by placing 
   	// them into an array of objects by using given array. 

   	// Both the answers to each questions and 
   	// the questions are shuffled using Math functions. 

    function generateQuiz(quizArray) {    	
    	for (let i = 0; i < quizArray.length; i++) {
    		answerPool[i]=quizArray[i].correctAnswer;
    	}    	
    	for (let i = 0; i < quizArray.length; i++) {
    		generated_Quiz[i] = {
    			question: quizArray[i].question,
    			choiceArray: [],
    			correctAnswer: quizArray[i].correctAnswer,
    			value: quizArray[i].value
    		};    		
    		let choiceArray = generated_Quiz[i].choiceArray;    		    		    		 		
    		while (choiceArray.length < 4) {
    			let randomNumber = Math.floor(Math.random() * answerPool.length);    			  				   			
    			if (i == randomNumber || choiceArray.indexOf(answerPool[randomNumber]) > -1) continue;    					
    			choiceArray[choiceArray.length] = answerPool[randomNumber];    			    			   			    													
    		}   			
    		let randomNumber = Math.floor(Math.random() * 3);
    		choiceArray[randomNumber]=quizArray[i].correctAnswer;
    	} 

	    for (let i = generated_Quiz.length - 1; i > 0; i--) {
	        let j = Math.floor(Math.random() * (i + 1));
	        let temp = generated_Quiz[i];
	        generated_Quiz[i] = generated_Quiz[j];
	        generated_Quiz[j] = temp;
	    }

	    storage.setItem('generated_Quiz', JSON.stringify(generated_Quiz));
	    storage.setItem('chosenQuizName', chosenQuizName);
	    createQuestion();
	}

	// generates quiz by using the question/answer objects from the
	// generated array.

	function createQuestion() {		
		selectQuizElem.hide();

		questionIndex === 0 ? 
			previousButton.hide():
			previousButton.show();

		questionIndex < generated_Quiz.length-1 ? 
			nextButton.show() && showResultsButton.hide():
			nextButton.hide() && showResultsButton.show();
		
		let question = generated_Quiz[questionIndex];
		answersElem.html('');
		header.html('Q'+(questionIndex+1)+'/'+generated_Quiz.length+': '+ question.question);
		for (i = 0; i < question.choiceArray.length; i++) {
			let answer = $('<div class="answer btn btn-lg btn-primary btn-block">');
			answer.append(question.choiceArray[i]);		
			answersElem.append(answer);	
		}

		answerElem = $('.answer');	
		answerElem.on('click', function() {
			answerElem.removeClass('selected');			
			$(this).addClass('selected');
			saveAnswer();
		})		
		selectedAnswers[questionIndex] !== undefined ?
			answerElem.each(function() {
				$(this).html() === selectedAnswers[questionIndex] ?
					$(this).addClass('selected'):''
			}):''

		storage.setItem('quizStage', 'inProgress');			   	
	    storage.setItem('questionIndex', questionIndex);	    
	}

	function saveAnswer () {
		selectedAnswers[questionIndex] = $('.selected').html() || undefined;		
		storage.setItem('quizStage', 'inProgress');
		storage.setItem('selectedAnswers', JSON.stringify(selectedAnswers));		
	}

	//generates results out of user input. 

	function generateResults() {		
		maxPoints = 0;
		pointsCollected = 0;
		correctAnswerCount = 0;
		resultTable.html('');

		previousButton.hide();
		showResultsButton.hide();
		submitResultsElem.hide();		
		answersElem.hide();
		selectQuizElem.hide();

		startOverButton.show();
		saveResultsButton.show();		

		isSaved == 1 ?
			showSavedResultsButton.show() &&
			saveResultsButton.hide():'';

		for (let i = 0; i < selectedAnswers.length; i++) {
			let question = generated_Quiz[i];				
			let row = $('<div>');
			let color = '';
			let answer = (selectedAnswers[i] == undefined ? 'No Answer.':selectedAnswers[i]);

			maxPoints += question.value;
			selectedAnswers[i] === question.correctAnswer ?
				(pointsCollected += question.value) &&
				correctAnswerCount++:
				color = 'wrong';			
			row.append(''+
				'Question: '+question.question+'<br/>'+
				'User Answer: <span class='+color+'>'+answer+'</span><br/>'+
				'Correct Answer: '+question.correctAnswer+'<br/>'+
				'Question Value: '+question.value+'<hr>	'
			);						
			resultTable.append(row);				
		}		
		answerElem.hide();
		resultTable.show();
		header.html('Results ('+chosenQuizName+')');
		resultTable.append(
			'<b>You answered '+correctAnswerCount+' questions out of '+
			selectedAnswers.length+' correctly and collected '+pointsCollected+' points out of '+maxPoints+'.</b>'
		);

		storage.setItem('quizStage', 'generateResults');
		storage.setItem('correctAnswerCount', correctAnswerCount);
	    storage.setItem('pointsCollected', pointsCollected);
	   	storage.setItem('maxPoints', maxPoints);	   	
	}

	// show the dialog to save results.

	function saveResults () {		
		resultTable.hide();		
		saveResultsButton.hide()
		selectQuizElem.hide();

		submitResultsElem.show();
		showResultsButton.show();
		startOverButton.show();	

		header.html('Save Results');

		storage.setItem('quizStage', 'saveResults');			
	}

	// submits user ganerated short version of user results to local storage.

	function submitResults () {
		let name = $('#name').val() || 'Someone';
		storage.getItem('userResults') == null ?
			storage.setItem('userResults', '[]'):'';
		let userResults = JSON.parse(storage.getItem('userResults'));
		let userResult = {
			quiz: chosenQuizName,
			name: name,
			correctAnswerCount: correctAnswerCount+'/'+selectedAnswers.length,
			pointsCollected: pointsCollected+'/'+maxPoints,
			date: Date()
		}			
		userResults.push(userResult);			
		storage.setItem('userResults', JSON.stringify(userResults));
		isSaved = 1;		

		showSavedResults();
		storage.setItem('quizStage', 'showSavedResults');
		storage.setItem('isSaved', isSaved);
	}

	//generates data from local storage to show previously saved results.

	function showSavedResults () {
		selectQuizElem.hide();
		showSavedResultsButton.hide();

		answersElem.show();
		showResultsButton.show();
		startOverButton.show();		

		header.html('Saved Results')
		answersElem.html('');
		resultTable.html('');

		if (JSON.parse(storage.getItem('userResults'))) {
			userResults = JSON.parse(storage.getItem('userResults'));
			$.each(userResults.reverse(), function () {			
				answersElem.append(
					this.name+' answered '+
					this.correctAnswerCount+' questions correctly and collected '+
					this.pointsCollected+' points on quiz "'+
					this.quiz+'" on '+
					this.date+'.<hr>'
				)
			})
		} else {
			answersElem.append('No results saved yet.')
		}
		
		storage.setItem('quizStage', 'showSavedResults');		
	}

	// restarts the quiz by reseting values.

	function startOver () {
		isSaved = 0;

		for (let i = storage.length-1	; i >= 0 ; i--){
   			let key = storage.key(i);   			  			  			
   			if (key !== 'userResults') storage.removeItem(key);
		}

		header.html('Select Quiz:');
		answersElem.html('');

		selectedAnswers = [];
		correctAnswerCount = 0;
		pointsCollected= 0;
		questionIndex = 0;
		maxPoints = 0;

		selectQuizElem.show();
		answersElem.show();

		showResultsButton.hide();
		resultTable.hide();
		startOverButton.hide();
		saveResultsButton.hide();
		
		submitResultsElem.hide();
		showSavedResultsButton.hide();
	}

	// buttons that initiate fuctions accordingly.

	function startButtonHandlers() {
		previousButton.on('click', function () {		
			saveAnswer();
			questionIndex--;
			createQuestion();
		})

		nextButton.on('click', function () {
			saveAnswer();			
			questionIndex++;
			createQuestion();		
		})

		showResultsButton.on('click', function () {
			saveAnswer();
			generateResults();
		})

		startOverButton.on('click', function() {
			startOver();	
		})

		saveResultsButton.on('click', function () {
			saveResults();
		})

		submitResultsElem.find('#submit').on('click', function () {
			submitResults();
			$(this).parent().hide();		
		})

		showSavedResultsButton.on('click', function () {
			showSavedResults();
		})

		startQuizA.on('click', function () {
			chosenQuizName = $(this).html();			   	
			generateQuiz(quiz_a); 
		});

		startQuizB.on('click', function () {			
			chosenQuizName = $(this).html();						
			generateQuiz(quiz_b); 			  		
		});
	}

	checkSession();

});